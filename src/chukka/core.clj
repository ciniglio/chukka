(ns chukka.core)

(defn do-something [x]
  (let [xr (map inc (range x))
        y (filter odd? xr)]
    (prn y)))
